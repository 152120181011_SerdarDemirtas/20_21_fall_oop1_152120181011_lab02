#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

/** Vector definition */
vector<int> parseInts(string str) {
    stringstream ss(str);
    vector<int> result;
    char charvalue;
    int tmp;

    while (ss >> tmp) {
        result.push_back(tmp);
        ss >> charvalue;
    }

    return result;
}

int main() {
    /** Getting string from the user and string definition */
    string string;
    cin >> string;
    vector<int> numbers = parseInts(string);
    for (int i = 0; i < numbers.size(); i++) {
        cout << numbers[i] << "\n";
    }

    return 0;
}