#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {

    /**  Values definitions */
    int size = 0;
    int* array;

    /** getting size of array */
    cin >> size;

    // setting size of array */
    array = new int[size];

    /** Getting array values from the user */
    for (int i = 0; i < size; i++) {
        cin >> array[i];
    }

    /** Printing array from the end of array to beginning */
    for (int i = size - 1; i >= 0; i--) {
        cout << array[i] << " ";
    }



    return 0;
}
