#include <iostream>
#include <string>
using namespace std;

int main() {

    /** Variables definitons */
    string a;
    string b;
    int lengthofa;
    int lengthofb;

    /** Getting strings */
    cin >> a;
    cin >> b;

    /** setting length value of strings */
    lengthofa = a.size();
    lengthofb = b.size();

    /** Printing operations */
    cout << lengthofa << " " << lengthofb << endl;
    cout << a + b << endl;

    /** Changing first index values and printing strings again */
    char temp;
    temp = a[0];
    a[0] = b[0];
    b[0] = temp;
    cout << a << " " << b << endl;



    return 0;
}