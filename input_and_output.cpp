#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {

    /** Variables definitions */
    int n1;
    int n2;
    int n3;

    /** Getting inputs from the user */
    cin >> n1 >> n2 >> n3;

    /** Writing the sum of three numbers  */
    cout << n1 + n2 + n3 << endl;


    return 0;
}
