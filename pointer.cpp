#include <stdio.h>

void update(int* a, int* b) {

    /** Necessary variables defitions */
    int number1 = 0;
    int number2 = 0;

    /** Setting values */
    if (*a >= *b) {
        number1 = *a;
        number2 = *b;
    }
    else {
        number1 = *b;
        number2 = *a;
    }

    /** New values */
    *a = number1 + number2;
    *b = number1 - number2;



}

int main() {
    int a, b;
    int* pa = &a, * pb = &b;

    scanf_s("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);

    return 0;
}