#include <iostream>
#include <sstream>
using namespace std;


/** Class implementation */
class Student {

 /** Variables definitions*/
private:
    int age=0;
    int standard=0;
    string firstname;
    string lastname;

/** Methods definitions*/
public:
    void set_age(int age) {
        this->age = age;
    }
    void set_standard(int standard) {
        this->standard = standard;
    }
    void set_first_name(string firstname) {
        this->firstname = firstname;
    }
    void set_last_name(string lastname) {
        this->lastname = lastname;
    }

    int get_age() {
        return age;
    }
    int get_standard() {
        return standard;
    }
    string get_first_name() {
        return firstname;
    }
    string get_last_name() {
        return lastname;
    }

    string to_string() {

        auto sage = std::to_string(age);
        auto sstandard = std::to_string(standard);

        string lastString = sage + "," + firstname + "," + lastname + "," + sstandard;

        return lastString;
    }


};

int main() {
    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard;

    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);

    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();

    return 0;
}