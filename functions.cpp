#include <iostream>
#include <cstdio>
using namespace std;

/** Max of four function implementation */
int max_of_four(int n1, int n2, int n3, int n4) {

    int max = n1;

    if (max < n2) {
        max = n2;
    }if (max < n3) {
        max = n3;
    }if (max < n4) {
        max = n4;
    }

    return max;
}

int main() {
    int a, b, c, d;
    scanf("%d %d %d %d", &a, &b, &c, &d);
    int ans = max_of_four(a, b, c, d);
    printf("%d", ans);

    return 0;
}