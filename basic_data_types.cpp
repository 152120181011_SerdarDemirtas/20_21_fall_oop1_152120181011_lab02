#include <iostream>
#include <cstdio>
using namespace std;

int main() {

    /** Variables definitions */
    int intvalue = 0;
    long longvalue = 0;
    char charvalue = ' ';
    float floatvalue = 0;
    double doublevalue = 0;

    /** Getting inputs from user */
    cin >> intvalue >> longvalue >> charvalue >> floatvalue >> doublevalue;

    /** Writing outputs */
    printf("%d\n", intvalue);
    printf("%ld\n", longvalue);
    printf("%c\n", charvalue);
    printf("%f\n", floatvalue);
    printf("%lf\n", doublevalue);


    return 0;
}