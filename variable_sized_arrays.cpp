#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {

    /** Variables definitions */
    int n, q;

    /** Getting n and q values */
    cin >> n >> q;

    /** Main part of the project */
    vector<vector<int>>array;
    for (int i = 0; i < n; i++) {
        int k;
        cin >> k;
        vector<int>elementsofarray;
        for (int j = 0; j < k; j++) {
            int l;
            cin >> l;
            elementsofarray.push_back(l);
        }
        array.push_back(elementsofarray);
    }
    for (int i = 0; i < q; i++) {
        int m;
        int n;
        cin >> m >> n;
        cout << array[m][n] << endl;
    }



    return 0;
}